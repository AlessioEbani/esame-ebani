﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jump : MonoBehaviour
{

    public float pushVel;
    private float delay;
    private float timer1=0;
    private float timer2 = 0;
    private float timer3 = 0;
    private bool beginTimer=false;
    private bool endTimer = false;
    private bool jumpTimer = false;
    private Rigidbody rb;
    public int totalJump;
    [SerializeField]
    private int jump;
    public float jumpSpeed;
    public float gravityMultiplier;

    private void Awake() {
        rb = GetComponent<Rigidbody>();
        jump = totalJump;
        rb.useGravity = false;
    }

    private void Update() {
        if (!GameManager.Instance.pause) {
            Jumps();
        }
        
    }
    private void FixedUpdate() {
        if (!GameManager.Instance.pause) {
            if (beginTimer) {
                timer1 += Time.deltaTime;
                if (timer1 > delay) {
                    if (jump > 0) {
                        beginTimer = false;
                        timer1 = 0;
                        StartJump();
                    }
                }
            }
            if (jumpTimer) {
                timer2 += Time.deltaTime;
                if (timer2 > delay) {
                    slowFall();
                }
            }
            if (endTimer) {
                timer3 += Time.deltaTime;
                if (timer3 > delay) {
                    endTimer = false;
                    timer3 = 0;
                    EndJump();
                }
            }
            if (jump <= 0) {
                var gravity = GameManager.Instance.globalGravity * Vector3.up;
                rb.AddForce(gravity, ForceMode.Acceleration);
            }
        } else {
            rb.velocity = Vector3.zero;
        }
    }


    private void Jumps() {
        if (Input.touchCount > 0) {
        Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began) {
                beginTimer=true;
            }
            if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved) {
                jumpTimer = true;
            }
            if (touch.phase == TouchPhase.Ended) {
                endTimer = true;
            }
            if (touch.phase == TouchPhase.Canceled) {
                endTimer = true;
            }
        }
        
    }

    void StartJump() {
        rb.velocity = Vector3.up * jumpSpeed;
    }
    void EndJump() {
        jump--;
    }
    void slowFall() {
        Vector3 gravity = GameManager.Instance.globalGravity * gravityMultiplier * Vector3.up;
        rb.AddForce(gravity, ForceMode.Acceleration);
    }

    private void OnDrawGizmos() {
        Gizmos.DrawCube(this.transform.position, transform.localScale / 2);
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Plane")) {
            jump = totalJump;
        }
        
    }

    private void OnCollisionStay(Collision collision) {
        if (collision.gameObject.CompareTag("Plane")) {
            jump = totalJump;
        }
        if (collision.gameObject.CompareTag("Pushable")) {
            rb.velocity = Vector3.right * pushVel ;
            //da mettere una condizione per smettere di pushare l'oggetto.
        }
    }


    public void SetDelay(float t) {
        delay = t;
    }


    private void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
