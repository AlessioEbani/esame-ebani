﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Orda : MonoBehaviour
{
    public float heigth=1.75f;
    public GameObject zombie;
    public Transform right;
    public Transform left;
    public Transform forward;

    public List<GameObject> orda=new List<GameObject>();
    //è necessario un array di rigidbody per non richiamarli ogni volta

    public bool changed = false;

    private void Start() {
        ChangeList();
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Z)) {
            AddZombie();
        }

        if (Input.GetKeyDown(KeyCode.X)) {
            ResetPosition();
        }

        if (Input.GetKeyDown(KeyCode.G)) {
            Godmode(GameManager.Instance.godMode);
        }
        
        if (changed) {
            changed = false;
            ChangeList();
        }
    }

    private float CalcolaTempo(GameObject z) {
        var space = right.position.x - z.transform.position.x;
        var vel = GameManager.Instance.globalVel;
        var time = space / vel;
        return time;
    }

    public void AddZombie() {
        var x = 0f;
        if (orda.Count < 5) {
            x = Random.Range(left.position.x+(right.position.x-left.position.x)/2, right.position.x);
        } else {
            x = Random.Range(left.position.x, right.position.x);
        }
        var position = new Vector3(x, heigth, Random.Range(right.position.z, forward.position.z));
        var newZombie = Instantiate(zombie, position, transform.rotation);
        newZombie.transform.parent = this.gameObject.transform;
        orda.Add(newZombie);
        changed = true;
    }
    

    private void ChangeList() {
        Sort();
        foreach (GameObject zombie in orda) {
            var zJump = zombie.GetComponent<Jump>();
            zJump.SetDelay(CalcolaTempo(zombie));
        }
    }

    public void ResetPosition() {
        foreach(GameObject zombie in orda) {
            var rb = zombie.GetComponent<Rigidbody>();
            rb.velocity = Vector3.zero;
            foreach(GameObject z in orda) {
                float x = 0;
                if (orda.Count < 5) {
                    x = Random.Range(left.position.x + (right.position.x - left.position.x) / 2, right.position.x);
                } else {
                    x = Random.Range(left.position.x, right.position.x);
                }
                var position = new Vector3(x, heigth, Random.Range(right.position.z, forward.position.z));
                z.transform.position = position;
            }
            changed = true;
        }
    }

    private void Sort() {
        orda.Sort((x, y) => y.transform.position.x.CompareTo(x.transform.position.x));
        orda[0].transform.position=right.transform.position;
    }

    public void RemoveZombie(GameObject z) {
        orda.Remove(z);
        if (orda.Count == 0) {
            GameManager.Instance.lose = true;
        }
        ResetPosition();
        Destroy(z);
    }

    public void Godmode(bool active) {
        foreach(GameObject z in orda) {
            if (!active) {
                z.layer = 9;
            } else {
                z.layer = 8;
            }
        }
        GameManager.Instance.godMode = !active;
    }
}
