﻿using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class UVScroller : MonoBehaviour {
    private Material material;
    private MeshRenderer meshRend;

    private void Awake() {
        meshRend = GetComponent<MeshRenderer>();
        material = meshRend.material;
    }

    private void Update() {
        if (!GameManager.Instance.pause) {
            var offset = material.GetTextureOffset("_MainTex");
            offset.x %= 1;
            offset.y %= 1;
            var dir = GameManager.Instance.globalDir;
            dir.Normalize();
            offset += GameManager.Instance.GetBackgroundVel() * GameManager.Instance.GetBackgroundDir();
            material.SetTextureOffset("_MainTex", offset);
        }
    }
}
