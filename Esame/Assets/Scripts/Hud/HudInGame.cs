﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HudInGame : MonoBehaviour
{
    private Orda orda;
    public TextMeshProUGUI ordeNumber;
    private void Awake() {
        orda = FindObjectOfType<Orda>();
    }

    private void Update() {
        ordeNumber.text = "x " + orda.orda.Count;
    }


}
