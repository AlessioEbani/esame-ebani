﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HUDMenu : MonoBehaviour
{
    public TextMeshProUGUI text;

    private void Update() {
        text.text = " " + GameManager.Instance.coins;
    }
}
