﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour
{
    public GameObject game;
    public GameObject pause;
    public GameObject lose;

    private void Start() {
        pause.SetActive(false);
        lose.SetActive(false);
    }


    private void Update() {
        if (GameManager.Instance.pause) {
            pause.SetActive(true);
        } else {
            pause.SetActive(false);
        }
        if (GameManager.Instance.lose) {
            lose.SetActive(true);
        } else {
            lose.SetActive(false);
        }
    }

    public void TogglePause() {
        GameManager.Instance.TogglePause();
    }

    public void StartGame() {
        GameManager.Instance.StartGame();
    }

    public void MainMenu() {
        GameManager.Instance.MainMenu();
    }
}
