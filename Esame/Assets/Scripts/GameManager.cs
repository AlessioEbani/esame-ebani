﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
    [HideInInspector]
    public float globalVel = 1;
    public float normalVel = 1;
    public float pushVel = 1;
    public float globalGravity = -9.81f;
    public Vector2 globalDir = Vector3.left;
    public float backgroundVelFactor;
    public bool godMode;
    public int coins;

    public bool pause = false;
    public bool lose = false;

    public void Reset() {
        lose = false;
        Time.timeScale = 1;
        Unpause();
        UnPush();
    }

    public Vector2 GetBackgroundDir() {
        var dir = globalDir.normalized;
        return new Vector2(-dir.x, -dir.y);
    }
    public float GetBackgroundVel() {
        return globalVel * backgroundVelFactor;
    }

    public Vector3 GetGlobalDir() {
        var dir = globalDir.normalized;
        return dir;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.R)) {
            StartGame();
        }
        if (lose) {
            Time.timeScale = 0;
        } else {
            Time.timeScale = 1;
        }
        
    }

    public void Push() {
        globalVel = pushVel;
    }

    public void UnPush() {
        globalVel = normalVel;
    }

    public void StartGame() { 
        Reset();
        Application.LoadLevel(1);
    }

    public void MainMenu() {
        Reset();
        //E' necessario dare dei nomi alle scene e effettuare i cambiamenti di scena con i nomi
        Application.LoadLevel(0);
    }

    public void Pause() {
        pause = true;
        Time.timeScale = 0;
    }

    public void Unpause() {
        pause = false;
        Time.timeScale = 1;
    }

    public void TogglePause() {
        if (pause) {
            Unpause();
        } else {
            Pause();
        }
    }
}
