﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public Transform point;
    public List<GameObject> prefabs = new List<GameObject>();

    private float timer;

    private void Update() {
        var spazio = this.transform.position.x - point.transform.position.y;
        var vel = GameManager.Instance.globalVel;
        var time = spazio / vel;
        timer += Time.deltaTime;
        if (timer > time) {
            var random = Random.Range(0, prefabs.Count);
            Instantiate(prefabs[random], this.transform.position, this.transform.rotation);
            timer = 0;
        }
    }


}
