﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleRunner : BaseRunner
{
    private Orda orda;

    private void Awake() {
        base.Awake();
        orda = FindObjectOfType<Orda>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            orda.RemoveZombie(other.gameObject);
            Destroy(this.gameObject);
        }
    }
}
