﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinManager : MonoBehaviour
{
    private CoinRunner[] coins;
    private int count;

    private void Awake() {
        coins = GetComponentsInChildren<CoinRunner>();
        count = coins.Length;
    }

    private void Update() {
        if (count <= 0) {
            Debug.Log("Hai preso tutte le monete");
        }
    }

    public void TookCoin() {
        count--;
    }
}
