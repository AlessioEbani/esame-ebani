﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{

    private Orda orda;

    private void Awake() {
        orda = FindObjectOfType<Orda>();
    }
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            orda.RemoveZombie(other.gameObject);
        } else {
            Destroy(other.gameObject);
        }
    }
}
