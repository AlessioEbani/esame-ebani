﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyable : MonoBehaviour
{
    private Orda orda;

    public int zombies;
    public int necessary;
    private int count = 0;

    private void Awake() {
        orda = FindObjectOfType<Orda>();
    }

    private void Update() {
        if (!GameManager.Instance.pause) {
            if (count >= necessary) {
                GameManager.Instance.UnPush();
                for (int i = 0; i < zombies; i++) {
                    orda.AddZombie();
                }
                //magari il reset andrebbe fatto con un lerp
                orda.ResetPosition();
                GameManager.Instance.UnPush();
                Destroy(transform.parent.gameObject);
            }
        }
    }

    private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Player")) {
            count++;
            GameManager.Instance.Push();
        }
    }

    private void OnCollisionExit(Collision collision) {
        if (collision.gameObject.CompareTag("Player")) {
            count--;
            if (count <= 0) {
                //da trovare un modo per capire se l'ostacolo viene saltato dopo un po' di push
                //StartCoroutine(Delay(3));
            }
            GameManager.Instance.UnPush();
        }
    }

    private IEnumerator Delay(int seconds) {
        yield return new WaitForSeconds(seconds);
        orda.ResetPosition();
    }

    private void OnDestroy() {
        GameManager.Instance.UnPush();
    }
}
