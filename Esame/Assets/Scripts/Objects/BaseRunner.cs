﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class BaseRunner : MonoBehaviour {
    protected Rigidbody rb;
    private Vector3 velDirection = new Vector3(0, 0, -1);
    public float actualVel = 0.5f;

    protected void Awake() {
        rb = GetComponent<Rigidbody>();
    }

    protected void Start() {
        UpdateVelocity();
    }

    protected void FixedUpdate() {
        if (!GameManager.Instance.pause) {
            velDirection = GameManager.Instance.GetGlobalDir();
            UpdateVelocity();
        } else {
            rb.velocity = Vector3.zero;
        }
    }

    public void UpdateVelocity() {
        rb.velocity = velDirection * actualVel * GameManager.Instance.globalVel; ;
    }
}
