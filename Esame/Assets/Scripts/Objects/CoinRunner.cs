﻿using UnityEngine;

public class CoinRunner : BaseRunner {

    CoinManager coinManager;

    private void Awake() {
        base.Awake();
        coinManager = FindObjectOfType<CoinManager>();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            coinManager.TookCoin();
            Destroy(this.gameObject);
        }
    }
}
